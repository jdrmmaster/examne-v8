<?php 
require_once '../../SharedKernell/Utils.php';

 
 class EmployeeController {
    protected $utils;
    public function __construct()  {}
    public function buscarEmpleados($action, $id){
        $this->utils= new Utils();
        $SQL = "SP_LISTAR_EMPLEADOS_PHP :Action, :id"; 
        $PrepareStatement = $this->utils->QueryPDO($SQL); 
        $PrepareStatement->bindParam(":Action", $action);
        $PrepareStatement->bindParam(":id", $id);
        $PrepareStatement->execute(); 
        $result= $PrepareStatement->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
 }


?>