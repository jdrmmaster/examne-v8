<?php
    header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

if (!isset($_SESSION)) {
    session_start();
} 
require_once '../../controllers/employee/EmployeeController.php';

$employeeController = new EmployeeController();
$id = $_GET["id"];
   
 
$RestSalaryConse = $employeeController->buscarEmpleados("3SALARIOS",$id);
$total = 0;
foreach ($RestSalaryConse as $key => $salary){
 
   $OtherIncome = ($salary["BaseSalary"] + $salary["Commission"]) * (($salary["BaseSalary"] + $salary["Commission"] * 8) /100) + $salary["Commission"];
   $total += $salary["BaseSalary"] + $salary["ProductionBonus"]  + ($salary["CompensationBonus"] * (($salary["CompensationBonus"] * 75) / 100))+$OtherIncome;
   
    
}
$total =  ($total /3);
$FinalRest = array(
    "listaSalarios"=>$RestSalaryConse,
    "total"=>$total,
);
echo json_encode($FinalRest);

?>