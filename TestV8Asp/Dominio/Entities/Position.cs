﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EXAMEN.Domain 
{
    public class Position : BaseEntity<int>
    {

        public int Id { get; set; }
        public string Description { get; set; }
    }
}
