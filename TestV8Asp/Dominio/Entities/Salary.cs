﻿using EXAMEN.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace EXAMEN.Domain
{
   public class Salary : BaseEntity<int> 
    {      
      
        public int Id { get; set; }
        public int EmployeeCode { get; set; }
        public string EmployeeName { get; set; } 
        public string EmployeeSurname { get; set; } 
        public int Year { get; set; }
        public int Month { get; set; }
        public int Division { get; set; }
        public string DivisionDesc { get; set; }
        public int Position { get; set; }
        public string PositionDesc  { get; set; }
        public int Office { get; set; }
        public string OfficeDesc { get; set; }
        public int Grade { get; set; } 
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
 
    }
}
