﻿using EXAMEN.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;


namespace EXAMEN.Domain
{
   
    public class Employee : BaseEntity<int> 
    {      
      
        public int Id { get; set; } 
        public string Name { get; set; } 
        public string Surname { get; set; }  
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public int Office { get; set; }
        public string OfficeDesc { get; set; }
        public int Position { get; set; }
        public string PositionDesc { get; set; }
        public int Division { get; set; }
        public string DivisionDesc { get; set; }
        public int Grade { get; set; }



    }
}
