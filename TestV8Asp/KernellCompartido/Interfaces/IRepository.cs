﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN.SharedKernel.Interfaces
{
    public interface IRepository<T>  
    {
        T BuscarPorId(T id);

        List<T> ListarTodos();

        T Guardar(T entidad);

        T Actualizar(T entidad);

        T Eliminar(T entidad);

    }
}
