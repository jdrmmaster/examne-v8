﻿using EXAMEN.Domain;
using EXAMEN.Infrastructure.SQLServer.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EXAMEN.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionController : ControllerBase
    {
        DivisionRepository divisionRepository = new DivisionRepository();
        // GET: api/<DivisionController>
        [Route("divisions")]
        [HttpGet]
        public IEnumerable<Division> Get()
        {
            return divisionRepository.ListarTodos();
        }

        // GET api/<DivisionController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<DivisionController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<DivisionController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<DivisionController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
