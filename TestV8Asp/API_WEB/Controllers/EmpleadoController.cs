﻿using EXAMEN.Domain;
using EXAMEN.Infrastructure.SQLServer.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc; 
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;
using System.Text.Json;

namespace WEB_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadoController : ControllerBase
    {
        EmployeeRepository EmpleadoRp = new EmployeeRepository();

        [Route("empleados")]
        [HttpGet()]
        public IEnumerable<Salary> Get()
        { 
            return EmpleadoRp.ListarEmpleadosConSalary();
        }

        [Route("empleadosFiltros")]
        [HttpGet()]
        public IEnumerable<Salary>  EmpleadosFiltros(string action, int office, int position, int division, int grade)
        {
            return EmpleadoRp.ListarEmpleadosFiltros(action, office, position, division, grade);
        }
        
        [Route("guardarEmployee")]
        [HttpPost]
        public async Task<IActionResult> PostGuardar(Employee employee)
        {
      
            EmpleadoRp.GuardarEmpleadoM(employee);
            return Ok(employee);
        }
         
        [Route("guardarSalario")]
        [HttpPost]
        public async Task<IActionResult> PostSalary(Salary salary)
        {
           var existe = EmpleadoRp.GuardarSalario(salary);
           return Ok(new { existe = existe });
        }
        [Route("actualizarSalario")]
        [HttpPost]
        public async Task<IActionResult> ActualizarSalary(Salary salary)
        {
             EmpleadoRp.actualizarSalario(salary);
            return Ok(new { existe = true });
        }


        [Route("buscarEmpleados")]
        [HttpGet]
        public async Task<IActionResult> buscarEmpleados(string search, int limit, int all)
        {  
            var listaEmpleados = EmpleadoRp.buscarEmpleados(search, limit, all);
            return Ok(listaEmpleados);
        }
 
        [Route("masivos")]
        [HttpPost()]
        public object GenerarempleadosMasivos()
        { 
            EmpleadoRp.GeneracionMasiva();

            return new { ok = true };
        }


    }

   
}
