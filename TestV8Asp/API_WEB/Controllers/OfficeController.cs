﻿using EXAMEN.Domain;
using EXAMEN.Infrastructure.SQLServer.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EXAMEN.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController : ControllerBase
    {
        OfficeRepository officeRepository = new OfficeRepository();
        // GET: api/<OfficeController>
        [Route("offices")]
        [HttpGet]
        public IEnumerable<Office> Get()
        {
            return officeRepository.ListarTodos();
        }

        // GET api/<OfficeController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<OfficeController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<OfficeController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<OfficeController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
