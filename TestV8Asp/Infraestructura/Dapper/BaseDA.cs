﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN.Infrastructure.SQLServer.Dapper
{
   public class BaseDA
    {
     
            public static string strError;
            private static string ConnectionString;
            public enum EnumSistema { CONTEST, CONT_PROD }
            public static SqlConnection Conectar(EnumSistema sistema)
            {
               
                try
                {
                    switch (sistema)
                    {
                        case EnumSistema.CONTEST:
                            ConnectionString = @"Data Source=DESKTOP-PA7JVKU\SQLEXPRESS;Initial Catalog=ExamenV8; Trusted_Connection=True;";
                            break;
                        case EnumSistema.CONT_PROD:
                            ConnectionString = "";
                            break;
                    }

                    SqlConnection Con = new SqlConnection(ConnectionString);
                    Con.Open();

                    return Con;
                }
                catch (Exception ex)
                {
                    strError = ex.Message;
                    return null;
                }
            }

            public async static Task<SqlConnection> ConectarAsync(EnumSistema sistema)
            {
                try
                {
                    switch (sistema)
                    {
                        case EnumSistema.CONTEST:
                            ConnectionString = ConfigurationManager.ConnectionStrings["SqlCon"].ConnectionString;
                            break;
                        case EnumSistema.CONT_PROD:
                            ConnectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                            break;
                    }

                    SqlConnection Con = new SqlConnection(ConnectionString);
                    await Con.OpenAsync();

                    return Con;
                }
                catch (Exception ex)
                {
                    strError = ex.Message;
                    return null;
                }
            }
        }
    
     
}
