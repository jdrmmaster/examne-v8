﻿using Dapper;
using EXAMEN.Domain;
using EXAMEN.Infrastructure.SQLServer.Dapper;
using EXAMEN.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN.Infrastructure.SQLServer.Repository
{
    public class DivisionRepository : BaseDA, IRepository<Division>
    {
        public Division Actualizar(Division entidad)
        {
            throw new NotImplementedException();
        }

        public Division BuscarPorId(Division id)
        {
            throw new NotImplementedException();
        }

        public Division Eliminar(Division entidad)
        {
            throw new NotImplementedException();
        }

        public Division Guardar(Division entidad)
        {
            throw new NotImplementedException();
        }

     
        public List<Division> ListarTodos()
        {

            using (SqlConnection con = Conectar(EnumSistema.CONTEST))
            {
                var sql = "SP_LISTAR_DIVISION"; 
                var result = (List<Division>)con.Query<Division>(sql,
                    commandType: System.Data.CommandType.StoredProcedure);

                return result;
            }
        }

        
    }
}
