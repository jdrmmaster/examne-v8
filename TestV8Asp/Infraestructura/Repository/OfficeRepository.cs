﻿using Dapper;
using EXAMEN.Domain;
using EXAMEN.Infrastructure.SQLServer.Dapper;
using EXAMEN.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN.Infrastructure.SQLServer.Repository
{
    public class OfficeRepository : BaseDA, IRepository<Office>
    {
        public Office Actualizar(Office entidad)
        {
            throw new NotImplementedException();
        }

        public Office BuscarPorId(Office id)
        {
            throw new NotImplementedException();
        }

        public Office Eliminar(Office entidad)
        {
            throw new NotImplementedException();
        }

        public Office Guardar(Office entidad)
        {
            throw new NotImplementedException();
        }

     
        public List<Office> ListarTodos()
        {

            using (SqlConnection con = Conectar(EnumSistema.CONTEST))
            {
                var sql = "SP_LISTAR_OFFICE"; 
                var result = (List<Office>)con.Query<Office>(sql,
                    commandType: System.Data.CommandType.StoredProcedure);

                return result;
            }
        }

        List<Office> IRepository<Office>.ListarTodos()
        {
            throw new NotImplementedException();
        }
    }
}
