﻿using Dapper;
using EXAMEN.Domain;
using EXAMEN.Infrastructure.SQLServer.Dapper;
using EXAMEN.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN.Infrastructure.SQLServer.Repository
{
    public class PositionRepository : BaseDA, IRepository<Position>
    {
        public Position Actualizar(Position entidad)
        {
            throw new NotImplementedException();
        }

        public Position BuscarPorId(Position id)
        {
            throw new NotImplementedException();
        }

        public Position Eliminar(Position entidad)
        {
            throw new NotImplementedException();
        }

        public Position Guardar(Position entidad)
        {
            throw new NotImplementedException();
        }

     
        public List<Position> ListarTodos()
        {

            using (SqlConnection con = Conectar(EnumSistema.CONTEST))
            {
                var sql = "SP_LISTAR_POSITION"; 
                var result = (List<Position>)con.Query<Position>(sql,
                    commandType: System.Data.CommandType.StoredProcedure);

                return result;
            }
        }

        
    }
}
