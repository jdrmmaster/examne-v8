﻿using Dapper;
using EXAMEN.Domain;
using EXAMEN.Infrastructure.SQLServer.Dapper;
using EXAMEN.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN.Infrastructure.SQLServer.Repository
{
    public class EmployeeRepository : BaseDA, IRepository<Employee>
    {
        public Employee Actualizar(Employee employee)
        {
            throw new NotImplementedException();
        }

        public Employee BuscarPorId(Employee id)
        {
            throw new NotImplementedException();
        }

        public Employee Eliminar(Employee employee)
        {
            throw new NotImplementedException();
        }

        public int RandomNumber(int ini, int end)
        {
            int n = new Random().Next(ini, end); 
            return n;
        }
        public void GeneracionMasiva()
        {

            try
            {

           
            var cantidadMeses = 6;
            var cantidadEmpelados = 100;
            var dat =   DateTime.Now;
            for(var e = 1;  e <= cantidadEmpelados; e++)
            {
                Employee empleado = new Employee();
                empleado.Name = "David " + e;
                empleado.Surname = "Marte " + e; 
                empleado.BeginDate = DateTime.Now;
                empleado.Birthday = DateTime.Now; 
                empleado.IdentificationNumber = (RandomNumber(1, 100)  + e).ToString();
                empleado.BaseSalary = RandomNumber(10000, 90000);
                empleado.Division = RandomNumber(1, 4);
                empleado.Office = RandomNumber(1, 4);
                empleado.Position = RandomNumber(1, 7);
                empleado.Grade = RandomNumber(1, 20);

                var empGuardado = GuardarEmpleadoM(empleado) as Employee;
                var b = 1;

                    for (int i = 1; i <= cantidadMeses; i++)
                    {
                        Console.WriteLine(i);
                        var fechaMesAtras = empleado.BeginDate.AddMonths(-i);

                        Salary salaryempleado = new Salary();
                        salaryempleado.Year = fechaMesAtras.Year;
                        salaryempleado.EmployeeName = empGuardado.Name; 
                        salaryempleado.EmployeeSurname = empGuardado.Surname;
                        salaryempleado.EmployeeCode = empGuardado.Id;
                        salaryempleado.Month = fechaMesAtras.Month;
                        salaryempleado.BeginDate = fechaMesAtras;
                        salaryempleado.Birthday = empGuardado.Birthday;
                        salaryempleado.Division = empGuardado.Division;
                        salaryempleado.Office = empGuardado.Office;
                        salaryempleado.Position = empGuardado.Position;
                        salaryempleado.Grade = empGuardado.Grade;
                        salaryempleado.IdentificationNumber = empGuardado.IdentificationNumber;
                        salaryempleado.BaseSalary = empGuardado.BaseSalary;
                        salaryempleado.ProductionBonus = RandomNumber(50, 100);
                        salaryempleado.CompensationBonus = RandomNumber(50, 100);
                        salaryempleado.Commission = RandomNumber(70, 150);
                        salaryempleado.Contributions = RandomNumber(120, 250);
                        var emps = GuardarSalario(salaryempleado);  
                        

                    }
                }
            }
            catch(Exception e)
            {
                var ex = e; 
            }
        }
        public Employee GuardarEmpleadoM(Employee employee)
        {
            try
            {

                using (SqlConnection con = Conectar(EnumSistema.CONTEST))
                {
                    using (SqlTransaction trann = con.BeginTransaction())
                    {
                        var sql = "SP_CREAR_EMPLEADO";

                        var result = con.Query<Employee>(
                              sql,
                              param: employee,
                              commandType: System.Data.CommandType.StoredProcedure,
                              transaction: trann,
                              commandTimeout: 120
                             );

                        trann.Commit();
                        employee.Id = result.ElementAt(0).Id;
                        return employee;


                    }
                }


                return employee;
            }
            catch (Exception ex)
            {

                return employee;
            }
        }
        public bool GuardarSalario(Salary salary)
        {
            try
            {

                using (SqlConnection con = Conectar(EnumSistema.CONTEST))
                {
                    using (SqlTransaction trann = con.BeginTransaction())
                    {
                        var sql = "SP_CREAR_SALARIO";

                        var result = con.Query<int>(
                              sql,
                              param: salary,
                              commandType: System.Data.CommandType.StoredProcedure,
                              transaction: trann,
                              commandTimeout: 120
                             );

                        trann.Commit();

                        var exist = result.ElementAt(0);
                        if (exist > 0) return true;  
                        else return false;  


                    }
                }


                return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public bool actualizarSalario(Salary salary)
        {
            try
            {

                using (SqlConnection con = Conectar(EnumSistema.CONTEST))
                {
                    using (SqlTransaction trann = con.BeginTransaction())
                    {
                        var sql = "SP_ACTUALIZAR_SALARIO";

                        var result = con.Query<int>(
                              sql,
                              param: salary,
                              commandType: System.Data.CommandType.StoredProcedure,
                              transaction: trann,
                              commandTimeout: 120
                             );

                        trann.Commit();

                        var exist = result.ElementAt(0);
                        if (exist > 0) return true;
                        else return false;


                    }
                }


                return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public Employee Guardar(Employee employee)
        {
            try
            {
               
                    using (SqlConnection con = Conectar(EnumSistema.CONTEST))
                    {
                         using (SqlTransaction trann = con.BeginTransaction())
                        {
                            var sql = "SP_CREAR_EMPLEADO";

                           var result = con.Query<Employee>(
                                 sql,
                                 param: employee,
                                 commandType: System.Data.CommandType.StoredProcedure,
                                 transaction: trann,
                                 commandTimeout: 120
                                );

                             trann.Commit();
                           


                         }
                    }

             
                return employee;
            }
            catch (Exception ex)
            {
               
                return employee;
            }
        }

      
        public List<Employee> buscarEmpleados(string search, int limit, int all)
        {
            try
            { 
                using (SqlConnection con = Conectar(EnumSistema.CONTEST))
                {
                    var sql = "SP_BUSCAR_EMPLEADOS";

                    var result = (List<Employee>)con.Query<Employee>(sql,
                        param: new { Buscar = search, Limit = limit, All = all},
                        commandType: System.Data.CommandType.StoredProcedure);

                    return result;
                }

            }
            catch (Exception e)
            {
                return new List<Employee>();
            }
        }
        public List<Salary> ListarEmpleadosConSalary()
        {

            using (SqlConnection con = Conectar(EnumSistema.CONTEST))
            {
                var sql = "SP_LISTAR_EMPLEADOS";

                var result = (List<Salary>)con.Query<Salary>(sql,
                    commandType: System.Data.CommandType.StoredProcedure);

                return result;
            }
        }

        public List<Salary> ListarEmpleadosFiltros(string action,int office, int position, int division, int grade)
        {

            using (SqlConnection con = Conectar(EnumSistema.CONTEST))
            {
                var sql = "FILTRAR_EMPLEADOS";

                var result = (List<Salary>)con.Query<Salary>(sql,
                    param: new {Action = action, Office = office, Position = position, Division = division, Grade = grade },
                    commandType: System.Data.CommandType.StoredProcedure);

                return result;
            }
        }
        public List<Employee> ListarTodos()
        {
 
           using (SqlConnection con = Conectar(EnumSistema.CONTEST))
            { 
                var sql = "SP_LISTAR_EMPLEADOS_ALL";

                var result = (List<Employee>)con.Query<Employee>(sql,  
                    commandType: System.Data.CommandType.StoredProcedure);

                return result;
            }
        }
    }
}
